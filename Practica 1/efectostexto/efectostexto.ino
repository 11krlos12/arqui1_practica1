#include <MD_Parola.h>
#include <MD_MAX72xx.h>
#include <SPI.h>

#define HARDWARE_TYPE MD_MAX72XX::FC16_HW
#define MAX_DEVICES 2
#define CLK_PIN   13
#define DATA_PIN  11
#define CS_PIN    10

/*
#-----------------------------------------------#
#                 Krlos López                   #
#                  201313894                    #
#             ♡ Debian + Gnome ♡                #
#-----------------------------------------------#
*/



MD_Parola miDisplay = MD_Parola(HARDWARE_TYPE, DATA_PIN, CLK_PIN, CS_PIN, MAX_DEVICES);

const int A = 2;
const int B = 3;
const int C = 4;
const int D = 5;
const int E = 6;
const int F = 7;
const int G = 8;

const int N = 7;
const int SEGMENTOS[N] = {A,B,C,D,E,F,G};

/*ÁNODO COMÚN*/
const int DIGITOS[10][N] = {
/*0*/ {0,0,0,0,0,0,1},
/*1*/ {1,0,0,1,1,1,1},
/*2*/ {0,0,1,0,0,1,0},
/*3*/ {0,0,0,0,1,1,0},
/*4*/ {1,0,0,1,1,0,0},
/*5*/ {0,1,0,0,1,0,0},
/*6*/ {0,1,0,0,0,0,0},
/*7*/ {0,0,0,1,1,1,1},
/*8*/ {0,0,0,0,0,0,0},
/*9*/ {0,0,0,0,1,0,0}
};
const int OFF = HIGH;

char *vectorLetrasOriginal[]={"*","P","1","-","G","R","U","P","O","1","4","-","S","E","C","C","I","O","N","A","|","B","*",};
char *vectorLetras;
String msjDefault = "*P1-GRUPO14-SECCION B*";

int deletreando = 0;
int longitud = 0; 
int potVal = 0;
int rango = 0;
int mov;
int estatico;
int dir;
int flag = 0;

void setup(void)
{
  miDisplay.begin();
  miDisplay.setIntensity(5);
  miDisplay.displayClear();

  deletreando = 0;
  longitud = msjDefault.length();
  Serial.begin(9600);

  // Borra todo el contenido del display
  for (int i=0; i<N; i++){
    pinMode(SEGMENTOS[i], OUTPUT);
    digitalWrite(SEGMENTOS[i], OFF);//apagar
  }
}


void loop(void)
{
  mov = analogRead(A1);
  dir = analogRead(A2);
  potVal = analogRead(A0);
  rango = map(potVal, 1, 1023, 4, 0);
  int disp = map(rango, 4, 0, 0, 4);
  print(disp + 1);

  if(Serial.available()) {
    msjDefault = "";
    msjDefault = Serial.readString();
    longitud = msjDefault.length();
    miDisplay.displayReset();
    deletreando = 0;
  }
/*
 * **********************************************************************************************************************
 *   Cambio de direccion para las 2 matrices
 * **********************************************************************************************************************
*/
  direccion(dir);
/*
 * **********************************************************************************************************************
 *    Activacion de 1 o 2 matrices led
 * **********************************************************************************************************************
*/
  if (mov == 1023)
  {
    mensajeAmbasMatrices();
    flag = 1; 
  }
  else if (mov == 0)
  {
    if (deletreando <= 23){
      miDisplay.displayText( vectorLetrasOriginal[deletreando], PA_RIGHT, 25, 1000, PA_NO_EFFECT, PA_NO_EFFECT);
      deletreando += 1;
      delay(400);
    }

    flag = 0;

    if(deletreando == longitud) {
      deletreando = 0;
    }
  }


  /*
   * **********************************************************************************************************************
   *   velocidades
   * **********************************************************************************************************************
  */  

  if(flag == 1){
    direccion(dir);
    int vel = 50 + (rango * 300);
    int slide_scroll_speed = map(vel, 1023, 0, 200, 15);
    miDisplay.setSpeed(slide_scroll_speed);
  }
  
  
  if (miDisplay.displayAnimate()) {
    miDisplay.displayReset();
  }

  
}

  /*
   * **********************************************************************************************************************
   *   Realizar la visualización en ambas matrices
   * **********************************************************************************************************************
  */  
void mensajeAmbasMatrices()
{
    if(flag == 1) {
      return;
    }
    miDisplay.displayClear();
    miDisplay.displayText(msjDefault.c_str(), PA_LEFT, 1500, 0, PA_SCROLL_LEFT, PA_SCROLL_LEFT);
}


  /*
   * **********************************************************************************************************************
   *   Cambio de direccion del texto en las matrices
   * **********************************************************************************************************************
  */  
void direccion(int cambio)
{
  if (cambio == 1023)
  {
    miDisplay.setTextEffect(PA_SCROLL_LEFT, PA_SCROLL_LEFT);
  }else if (cambio == 0)
  {
    miDisplay.setTextEffect(PA_SCROLL_RIGHT, PA_SCROLL_RIGHT);
  }
}


  /*
   * **********************************************************************************************************************
   *   Imprimir mensaje en matrices
   * **********************************************************************************************************************
  */  
void print(int d){
  for (int i=0; i<N; i++){
    digitalWrite(SEGMENTOS[i], DIGITOS[d][i]);
  }
}
