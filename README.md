# PRACTICA 1 - ARQUITECTURA COMPUTACIONAL 1
# USAC - GUATEMALA



# Composición del circuito y especificaciones
<div>
<p style = 'text-align:center;'>
<img src="./Imagenes/Dibujo1.png" alt="JuveYell" width="1000px">
</p>
</div>


# Especificaciones Técnicas


* Realizado en Sistema Operativo Windows 10
<div>
<p style = 'text-align:center;'>
<img src="./Imagenes/SO.PNG" alt="JuveYell" width="700px">
</p>
</div>



* Simulador Proteus Version 8.10 
<div>
<p style = 'text-align:center;'>
<img src="./Imagenes/proteus.PNG" alt="JuveYell" width="300px">
</p>
</div>


* Arduino 
<div>
<p style = 'text-align:center;'>
<img src="./Imagenes/arduino.PNG" alt="JuveYell" width="400px">
</p>
</div>

* Librerias necesarias para arduino, Incluirlas desde el gestor de librerias de arduino

<div>
<p style = 'text-align:center;'>
<p>
<img src="./Imagenes/gestorLibreria.PNG" alt="JuveYell" width="500px">
</p>
<img src="./Imagenes/librerias.PNG" alt="JuveYell" width="200px">
</p>
</div>

